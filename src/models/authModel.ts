import { User } from "./userModel";

export type Auth = {
  email: string;
  password: string;
};

export type TokenAuthentication = {
  token: string;
  token_type: string;
  user: User;
};
