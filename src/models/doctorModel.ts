export type DoctorForm = {
  id?: number | "";
  user_id: number | "";
  document: string;
  identification_id: string;
  surnames: string;
  names: string;
  birthday: Date;
  phone_code: string;
  phone: string;
  coach: number;
  status: number;
  email: string;
  password: string;
  password_confirmation: string;
};
