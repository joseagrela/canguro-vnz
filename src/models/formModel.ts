import { User } from "./userModel";

export type Form = {
  remaining_biophysics: number;
  remaining_biochemistry: number;
  remaining_orthomolecular: number;
  remaining_genetics: number;
};

export type FormModel = {
  id: number;
  user_id: number;
  type: number;
  quantity: number;
  remaining: number;
  user: User;
  created_at: Date;
  updated_at: Date;
};

export type TestForms = {
  id: number;
  test_id: number;
  name: string;
  translation: string;
  relative_value: string;
  long: string;
  high: string;
  width: string;
  absolute_value: string;
  created_at: Date;
  updated_at: Date;
};

export type FormType = {
  id?: number | "";
  user_id: number | "";
  type: number | "";
  name: string;
  quantity: number | "";
  remaining: number | "";
};

export type TestTypeForm = {
  id?: number | "";
  name: string;
  translate: string;
  relative_value: number | "";
  dimensions: boolean;
  long?: number | "";
  high?: number | "";
  width?: number | "";
  absolute_value: number | "";
};
