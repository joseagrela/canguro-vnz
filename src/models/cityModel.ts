import { State } from "./stateModel";

export type City = {
  id: number;
  state_id: number;
  name: string;
  overriding: number;
  state: State;
};
