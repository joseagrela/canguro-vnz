import { City } from "./cityModel";

export type State = {
  id: number;
  name: string;
  iso_3166: string;
  cities: City[];
};
