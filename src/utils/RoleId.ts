const RoleId = Object.freeze({
  ROOT: 1,
  DOCTOR: 2,
  COACH: 3,
  USER: 4,

  getDisplayName(role: number) {
    switch (role) {
      case RoleId.ROOT:
        return "Administrador";
      case RoleId.DOCTOR:
        return "Doctor";
      case RoleId.COACH:
        return "Coach";
      case RoleId.USER:
        return "Usuario";
      default:
        return "";
    }
  },
});

export default RoleId;
