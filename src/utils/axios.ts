import axios, {
  AxiosRequestConfig,
  AxiosInstance,
  AxiosResponse,
  AxiosError,
} from "axios";
import { store } from "store";

const { REACT_APP_API_URL: baseURL } = process.env;

const lugoService: AxiosInstance = axios.create({
  baseURL,
});

lugoService.interceptors.request.use((config: AxiosRequestConfig) => {
  config.headers.common["Authorization"] = `Bearer ${
    store.getState().token?.token
  }`;

  return config;
});

lugoService.interceptors.response.use(
  (response: AxiosResponse) => {
    return response;
  },
  (error: AxiosError) => {
    return new Promise(function (resolve, reject) {
      if (error?.response) {
        if (
          error.response.status === 500 &&
          error.response.data.message === "The token has been blacklisted"
        ) {
          return reject(error);
        } else if (
          error.response.status === 401 &&
          error.response.data.message === "Token not provided"
        ) {
          return reject(error);
        } else if (
          error.response.status === 403 &&
          error.response.data.message === "User status inactive"
        ) {
          store.dispatch({ type: "Token/REMOVE" });
          store.dispatch({ type: "User/REMOVE" });
        } else if (
          error.response.status === 401 &&
          error.response.data.message === "Token has expired"
        ) {
          // const retryOriginalRequest = authService.refresh(res => {
          //     originalRequest.headers.common['Authorization'] = 'Bearer ' + res['access_token']
          //     resolve(axios(originalRequest))
          // });
          // return retryOriginalRequest
        } else if (
          error.response.status === 500 &&
          error.response.data.message === "The token has been blacklisted"
        ) {
          return reject(error);
        } else if (
          error.response.status === 422 &&
          error.response.data.message === "The given data was invalid."
        ) {
          return reject(error);
        } else if (
          error.response.status === 500 &&
          error.response.data.message ===
            "Token has expired and can no longer be refreshed"
        ) {
          return reject(error);
        } else if (error.response.status === 500) {
          return reject(error);
        } else {
          return reject(error);
        }
      }
      return reject(error);
    });
  }
);

export default lugoService;
