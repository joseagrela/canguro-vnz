import ROLES from "./RoleId";

export const SidebarMenu = [
  {
    display_name: "Nosotros",
    path: "/admin/us",
    roles: [ROLES.ROOT, ROLES.DOCTOR, ROLES.COACH],
    submodules: [],
  },
  {
    display_name: "Contacto",
    path: "/admin/doctors",
    roles: [ROLES.ROOT],
    submodules: [],
  },
  {
    display_name: "Perfil",
    path: "/admin/profile",
    roles: [ROLES.ROOT, ROLES.DOCTOR, ROLES.COACH],
    submodules: [],
  },
];
