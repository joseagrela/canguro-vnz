import { combineReducers } from "redux";
import { TypedUseSelectorHook, useSelector } from "react-redux";
import { user } from "./user";
import { token } from "./token";
import { form } from "./form";

export const rootReducer = combineReducers({
  user,
  token,
  form,
});

export type RootState = ReturnType<typeof rootReducer>;

export const useTypedSelector: TypedUseSelectorHook<RootState> = useSelector;

export default rootReducer;
