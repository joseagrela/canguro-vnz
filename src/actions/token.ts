import { Token } from "models";
import { Action } from "redux";

export interface SetTokenAction extends Action<"Token/SET"> {
  token: Token;
}

export type RemoveTokenAction = Action<"Token/REMOVE">;
export type TokenAction = SetTokenAction | RemoveTokenAction;
export type TokenState = Token | null;

export const setToken = (token: Token): TokenAction => ({
  type: "Token/SET",
  token
});

export const removeToken = (): TokenAction => ({
  type: "Token/REMOVE"
});
