import { Form } from "models";
import { Action } from "redux";

export interface SetFormAction extends Action<"Form/SET"> {
  form: Form;
}

export type RemoveFormAction = Action<"Form/REMOVE">;
export type FormAction = SetFormAction | RemoveFormAction;
export type FormState = Form | null;

export const setForm = (form: Form): FormAction => ({
  type: "Form/SET",
  form,
});

export const removeForm = (): FormAction => ({
  type: "Form/REMOVE",
});
