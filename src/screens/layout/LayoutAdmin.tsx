import React, { ReactNode } from "react";
import axios, { CancelTokenSource } from "axios";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { connect, ConnectedProps } from "react-redux";

import { Globals, SidebarMenu } from "utils";
import { Header, Sidebar } from "components";
import { AuthService } from "services";

import { RootState } from "reducers";

const mapState = (state: RootState) => ({
  user: state.user,
});

const mapDispatch = {
  dispatchUser: () => ({ user: null, type: "User/REMOVE" }),
  dispatchToken: () => ({ token: null, type: "Token/REMOVE" }),
};

const connector = connect(mapState, mapDispatch);

export type Menu = {
  display_name: string;
  path: string;
  roles: Array<any>;
};

export type State = {
  leftOpen: boolean;
  items: Menu[];
};

const initialState = {
  leftOpen: true,
  items: SidebarMenu,
};

type Props = ConnectedProps<typeof connector> & RouteComponentProps;

class LayoutAdmin extends React.PureComponent<Props, State> {
  public state: State;
  public source: CancelTokenSource;

  constructor(props: Props) {
    super(props);

    this.state = initialState;
    const cancelToken = axios.CancelToken;

    this.source = cancelToken.source();
  }

  logout = () => {
    const { dispatchUser, dispatchToken, history } = this.props;

    Globals.logout("¿Deseas salir?", async () => {
      await AuthService.logout(this.source);

      dispatchUser();
      dispatchToken();

      history.replace("/login");
    });
  };

  toggleSidebar = (event: any) => {
    let key: any = `${event.currentTarget.parentNode.id}Open`;
    // @ts-ignore
    this.setState({ [key]: !this.state[key] });
  };

  render(): ReactNode {
    const leftOpen = this.state.leftOpen ? "open" : "closed";
    const { user, history, children } = this.props;
    const username = user?.name||'admin' ;
    const { items } = this.state;

    const role_id = !!user ? user : 0;
    const menu = filterMenu(items,1);

    return (
      <React.Fragment>
        <Header username={username} />
        <div id="layout">
          <Sidebar
            menu={menu}
            pathname={history.location.pathname}
            status={leftOpen}
            onCloseSidebar={this.toggleSidebar}
            onPressLogout={this.logout}
          />
          <div className={`container-router ${leftOpen}`}>{children}</div>
        </div>
      </React.Fragment>
    );
  }
}

function filterMenu(menu: Menu[], roleId: number) {
  return menu.filter(
    ({ roles }: { roles: Array<any> }) => !roles || roles.includes(roleId)
  );
}

export default withRouter(connector(LayoutAdmin));
