import { Test } from "models";

export type TestProps = {
  test: Test;
  toView: (test: Test) => void;
};
