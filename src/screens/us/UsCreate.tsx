import React, { ReactNode } from "react";
import { RouteComponentProps } from "react-router-dom";
import axios, { CancelTokenSource } from "axios";

import { UsModel } from "models";


import { Textarea, Button } from "components";
import { TitleModal, PageTitle } from "components";
import { handlerError, showSuccess } from "utils";
import { initialState, State } from "./FormState";
import UsService from "services/modules/usService";

type Props = RouteComponentProps;

class UsCreate extends React.PureComponent<Props,State> {
  public state: State;
  public source: CancelTokenSource;

  constructor(props: Props) {
    super(props);

    this.state = initialState;

    const cancelToken = axios.CancelToken;
    this.source = cancelToken.source();
  }

  handleChange = (key: string) => {
    return (event: React.ChangeEvent<HTMLInputElement>) => {
      const { value, name } = event.currentTarget;
      this.setState((prevState: any) => {
        return {
          ...prevState,
          [key]: value
        };
      });
    };
  };

  handleSubmit = async (
    event: React.FormEvent<HTMLFormElement>
  ): Promise<void> => {
    event.preventDefault();
    const {mision,moral,vision,kangaroo_to_world,identity_kangaroo} = this.state
    if (this.state.isSubmit) {
      return;
    }

    this.setState({
      isSubmit: true,
    });

    const us: UsModel = {
      map:"",
      mision,
      vision,
      moral,
      kangaroo_to_world,
      identity_kangaroo
    };


    UsService.store(us, this.source)
      .then(() => {
        showSuccess();

        setTimeout(() => {
          this.props.history.push("/admin/stories");
        }, 1000);
      })
      .catch(handlerError)
      .finally(() => this.setState({ isSubmit: false }));
  };

  render(): ReactNode {
    const { isSubmit, mision, vision, moral, kangaroo_to_world,identity_kangaroo } = this.state;

    return (
      <div className="container">
        <PageTitle backUrl="/admin/stories">Edición de nosotrs</PageTitle>
        <div className="row justify-content-center">
          <div className="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
            <TitleModal>Nosotros</TitleModal>
            <div className="card">
              <div className="card-body">
                <form
                  onSubmit={this.handleSubmit}
                  noValidate
                  autoComplete="off"
                >
                  <div className="row">
                    <div className="col-12 col-sm-12 col-md">
                     <Textarea
                      label = {'Misión'}
                      name = {'mission'}
                      value = {mision}
                      onChange = {this.handleChange("mision")}
                      /> 
    
                     
                     </div>
                     </div>
                     <div className="row">
                     <div className="col-12 col-sm-12 col-md">
                      <Textarea
                        label = {'Visión'}
                        name = {'vision'}
                        value = {vision}
                        onChange = {this.handleChange("vision")}
                      />
                    </div>
                    </div>

                    <div className="row">
                    <div className="col-12 col-sm-12 col-md">
                      <Textarea
                        label = {'Moral'}
                        name = {'moral'}
                        value = {moral}
                        onChange = {this.handleChange("moral")}
                      />
                    </div>
                    </div>

                    <div className="row">
                    <div className="col-12 col-sm-12 col-md">
                      <Textarea
                        label = {'Canguro en el mundo'}
                        name = {'kangaroo_to_world'}
                        value = {kangaroo_to_world}
                        onChange = {this.handleChange("kangaroo_to_world")}
                      />
                    </div>
                    </div>
                  <div className="row">
                    <div className="col-12 col-sm-12 col-md">
                      <Textarea
                        label = {'Diferentes identidades Canguro '}
                        name = {'identity_kangaroo '}
                        value = {identity_kangaroo}
                        onChange = {this.handleChange("identity_kangaroo")}
                      />


                    </div>
                    </div>
                  <div className="text-center row">
                    <div className="col col-sm">
                      <Button
                        submitted={isSubmit}
                        block
                        type="submit"
                        className="btn-orange shadow"
                      >
                        Guardar
                      </Button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default UsCreate;
