export type State = {
    isSubmit: boolean;
    error: string;
    mision:string,
    moral:string,
    vision:string,
    kangaroo_to_world: string,
    identity_kangaroo: string
  };

export const initialState: State = {
    isSubmit: false,
    error: "",
    mision:"",
    moral:"",
    vision:"",
    kangaroo_to_world: "",
    identity_kangaroo: ""
  };
  