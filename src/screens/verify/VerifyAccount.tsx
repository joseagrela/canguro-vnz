import React, { ReactNode } from "react";
import { RouteComponentProps } from "react-router-dom";
import { AuthService } from "services";
import { handlerError } from "functions";
import { Card } from "components";

type State = {
  isLoading: boolean;
  resText: string;
  error: boolean;
};

type Props = RouteComponentProps<{
  token: string;
  id: string;
}>;

class VerifyAccount extends React.PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      isLoading: true,
      resText: "",
      error: false
    };
  }

  componentDidMount() {
    const { match } = this.props;

    AuthService.verifyAccount({
      id: match.params.id,
      token: match.params.token
    })
      .then((response: string) => {
        this.setState({ resText: response });
      })
      .catch(error => {
        handlerError(error);
        this.setState({ error: true });
      })
      .finally(() => this.setState({ isLoading: false }));
  }

  render(): ReactNode {
    const { isLoading, resText, error } = this.state;

    return (
      <main>
        <div className="register-wrapper-left" />
        <div className="register-background">
          <div className="wrapper-content align-items-center">
            <div className="container">
              <div className="row justify-content-center">
                <Card className="wrapper-register">
                  <div className="text-center row">
                    <div className="col col-md col-lg">
                      {isLoading ? (
                        <div
                          className="spinner-border text-orange"
                          role="status"
                        >
                          <span className="sr-only">Loading...</span>
                        </div>
                      ) : (
                        <React.Fragment>
                          <h2 className="text-orange font-weight-bold">
                            {!error
                              ? "¡Enhorabuena!"
                              : "No se pudo verificar tu cuenta"}
                          </h2>
                          <p className="text-center mb-4">
                            <small>
                              {resText
                                ? resText
                                : "Hubo un error al tratar de verificar tu cuenta, consulta con el administrador para mas información"}
                            </small>
                          </p>
                        </React.Fragment>
                      )}
                    </div>
                  </div>
                </Card>
              </div>
            </div>
          </div>
          <div className="login-background-footer"></div>
        </div>
      </main>
    );
  }
}

export default VerifyAccount;
