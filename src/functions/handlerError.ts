import Swal from "sweetalert2";

export const INTERNAL_SERVER_ERROR = 500;
export const NOT_FOUND_ERROR = 404;
export const UNAUTHORIZED = 401;
export const UNPROCESSABLE_ENTITY = 422;
export const FORBIDDEN = 403;

export const handlerError = (error: any) => {
  const { message, response } = error;
  console.log(message, response, error, "ERROR");

  switch (response?.status) {
    case NOT_FOUND_ERROR:
      showError("No hemos encontrado lo que buscaba.");
      return;
    case INTERNAL_SERVER_ERROR:
      showError("Ha ocurrido un error interno.");
      return;
    case UNAUTHORIZED:
      showError("Usuario no autorizado.");
      return;
    case UNPROCESSABLE_ENTITY:
      showError(response.data[0]);
      return;
    case FORBIDDEN:
      showError("Usuario desactivado.");
      return;
  }

  if (isNewtworkError(message)) {
    showError("Revise su conexión");
    return;
  }
};

const showError = (text: string) => {
  Swal.fire({
    title: "",
    text: text,
    type: "error",
    showCancelButton: false,
    confirmButtonColor: "#f2692a"
  });
};

const isNewtworkError = (message: string): boolean =>
  message.indexOf("Network Error") > -1;
