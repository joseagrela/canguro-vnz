import AuthService from "./modules/authService";
import StateService from "./modules/stateService";
import FormService from "./modules/formService";
import RangeService from "./modules/rangeService";
import BoardService from "./modules/boardService";
import TestService from "./modules/testSetvice";

export {
  AuthService,
  StateService,
  FormService,
  RangeService,
  BoardService,
  TestService,
};
