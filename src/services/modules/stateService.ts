import apiService from "./apiService";
import { AxiosError, AxiosResponse, CancelToken } from "axios";
import { State } from "models";

type Source = {
  token: CancelToken;
};

class StateService {
  static getStateAndCity = (source: Source) => {
    return new Promise<State[]>((resolve, reject) => {
      apiService
        .get("state/city", {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<State[]>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };
}

export default StateService;
