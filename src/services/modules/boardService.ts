import apiService from "./apiService";
import { AxiosError, AxiosResponse, CancelToken } from "axios";
import { Board } from "models";

type Source = {
  token: CancelToken;
};

class RangeService {
  static boards = (formType: number, source: Source) => {
    return new Promise<Board[]>((resolve, reject) => {
      apiService
        .get(`boards?form=${formType}`, {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<Board[]>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };
}

export default RangeService;
