import apiService from "./apiService";
import { AxiosError, AxiosResponse, CancelToken } from "axios";
import { UsModel } from "models";

type Source = {
  token: CancelToken;
};

class UsService {
  
    static store = (usModel: UsModel, source: Source) => {
        return new Promise<UsModel>((resolve, reject) => {
            apiService
            .post("us", usModel, {
                cancelToken: source.token,
            })
            .then(
                (response: AxiosResponse<UsModel>) => resolve(response?.data),
                (error: AxiosError) => reject(error)
            );
        });
        };  
}

export default UsService;
