import apiService from "./apiService";
import { AxiosError, AxiosResponse, CancelToken } from "axios";
import { User, FormType, TestTypeForm } from "models";

type Source = {
  token: CancelToken;
};

class FormService {
  static storeBiophysics = (
    testForm: TestTypeForm[],
    form: {
      userId: number;
      biological: number;
      differential: number;
      gender: number;
    },
    source: Source
  ) => {
    return new Promise<User>((resolve, reject) => {
      apiService
        .post(
          "forms/biophysics",
          {
            form: JSON.stringify(testForm),
            ...form,
          },
          {
            cancelToken: source.token,
          }
        )
        .then(
          (response: AxiosResponse<User>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static storeBiochemistry = (
    testForm: TestTypeForm[],
    form: {
      userId: number;
      biochemistry: number;
      differential: number;
    },
    source: Source
  ) => {
    return new Promise<User>((resolve, reject) => {
      apiService
        .post(
          "forms/biochemistry",
          {
            form: JSON.stringify(testForm),
            ...form,
          },
          {
            cancelToken: source.token,
          }
        )
        .then(
          (response: AxiosResponse<User>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static storeOrthomolecular = (
    testForm: TestTypeForm[],
    form: {
      userId: number;
      chronological: number;
      orthomolecular: number;
      differential: number;
    },
    source: Source
  ) => {
    return new Promise<User>((resolve, reject) => {
      apiService
        .post(
          "forms/orthomolecular",
          {
            form: JSON.stringify(testForm),
            ...form,
          },
          {
            cancelToken: source.token,
          }
        )
        .then(
          (response: AxiosResponse<User>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static storeGenetics = (
    testForm: TestTypeForm[],
    form: {
      userId: number;
      chronological: number;
      genetics: number;
      differential: number;
      nat: number;
      cyp2d6: number;
      cyp2c19: number;
      cyp2c9: number;
    },
    source: Source
  ) => {
    return new Promise<User>((resolve, reject) => {
      apiService
        .post(
          "forms/genetics",
          {
            form: JSON.stringify(testForm),
            ...form,
          },
          {
            cancelToken: source.token,
          }
        )
        .then(
          (response: AxiosResponse<User>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };

  static update = (form: FormType[], userId: number, source: Source) => {
    return new Promise<User>((resolve, reject) => {
      apiService
        .put(`forms/${userId}`, form, {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<User>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };
}

export default FormService;
