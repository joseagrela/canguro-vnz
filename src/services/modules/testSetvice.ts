import apiService from "./apiService";
import { AxiosError, AxiosResponse, CancelToken } from "axios";
import { Tests } from "models";

type Source = {
  token: CancelToken;
};

class TestService {
  static tests = (id: number, page: number, source: Source) => {
    return new Promise<Tests>((resolve, reject) => {
      apiService
        .post(
          `tests?page=${page}`,
          { id: id },
          {
            cancelToken: source.token,
          }
        )
        .then(
          (response: AxiosResponse<Tests>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };
}

export default TestService;
