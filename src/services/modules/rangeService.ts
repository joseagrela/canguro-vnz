import apiService from "./apiService";
import { AxiosError, AxiosResponse, CancelToken } from "axios";
import { Range } from "models";

type Source = {
  token: CancelToken;
};

class RangeService {
  static ranges = (source: Source) => {
    return new Promise<Range[]>((resolve, reject) => {
      apiService
        .get("ranges", {
          cancelToken: source.token,
        })
        .then(
          (response: AxiosResponse<Range[]>) => resolve(response?.data),
          (error: AxiosError) => reject(error)
        );
    });
  };
}

export default RangeService;
