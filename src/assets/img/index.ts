import logo from "./logo.png";
import loginLeft from "./login-left.png";
import authBackground from "./auth-background.jpg";

export { logo, loginLeft, authBackground };
