import React, { ReactNode } from "react";
import { Switch, Redirect, Link } from "react-router-dom";
import { RouteProps, RouteComponentProps } from "react-router-dom";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Provider, connect } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";

// Screens
import Login from "screens/login/Login";
import Home from "screens/home/home";

import RestorePassword from "screens/restore/RestorePassword";
import VerifyAccount from "screens/verify/VerifyAccount";


// Screens Tests
import TestList from "screens/tests/TestList";

// Layout
import { PrivateRoute, Icon } from "components";

// Styles
import "bootstrap/dist/css/bootstrap.min.css";
import "font-awesome/css/font-awesome.min.css";
import "./assets/app.scss";

// Redux
import { store, persistor } from "./store";
import { RootState } from "reducers";
import { UserState } from "actions";

import "moment/locale/es";
import UsCreate from "screens/us/UsCreate";

const App: React.FC<{}> = () => {
  const { REACT_APP_PATH: basename } = process.env;
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Router basename={basename}>
          <Switch>
            <Route 
              exact 
              path="/verify/:token/:id" 
              component={VerifyAccount} 
            />

            <Route
              exact
              path="/reset-password/:token/:id"
              component={RestorePassword}
            />

            <GuestRoute 
              exact 
              path="/login" 
              component={Login} 
            />

            <PrivateRoute 
              exact 
              path="/admin" 
              component={Home} 
            />

            <PrivateRoute 
              exact 
              path="/admin/us" 
              component={UsCreate} 
            />
        
            <PrivateRoute
              exact
              path="/admin/tests/:id"
              component={TestList}
            />

            <Redirect path="/" to="/login" />
            <PrivateRoute component={PageNotFound} />
          </Switch>
        </Router>
      </PersistGate>
    </Provider>
  );
};

interface GuestRouteProps extends RouteProps {
  component:
    | React.ComponentType<RouteComponentProps<any>>
    | React.ComponentType<any>;
}

type RenderComponent = (props: RouteComponentProps<any>) => React.ReactNode;

const mapState = (state: RootState) => ({
  user: state.user,
});

class GuestRouteComponent extends Route<
  GuestRouteProps & {
    user: UserState;
  }
> {
  render(): ReactNode {
    const { component: Component, ...rest }: GuestRouteProps = this.props;
    const { user: isAuthenticated } = this.props;

    const renderComponent: RenderComponent = (props) =>
      !!isAuthenticated ? <Redirect to="/admin" /> : <Component {...props} />;

    return <Route {...rest} render={renderComponent} />;
  }
}

export const GuestRoute = connect(mapState)(GuestRouteComponent);

const PageNotFound = () => (
  <div className="container">
    <div className="row">
      <div className="col bg-white">
        <div className="h-100 p-5 text-center">
          <h1 className="text-center">Página no encontrada</h1>
          <p className="text-center">No hemos encontrado lo que buscabas.</p>
          <Icon name="exclamation-circle" style={{ fontSize: "5rem" }} />
          <p>
            <br />
            <br />
            <Link to="/admin">Volver al inicio</Link>
          </p>
        </div>
      </div>
    </div>
  </div>
);

// const PageUnderConstruction = () => (
//   <div className="container">
//     <div className="row">
//       <div className="col bg-white">
//         <div className="h-100 p-5 text-center">
//           <h1 className="text-center">Página en construcción</h1>
//           <p className="text-center">
//             Esta página se encuentra en construcción.
//           </p>
//           <Icon name="cogs" style={{ fontSize: "5rem" }} />
//         </div>
//       </div>
//     </div>
//   </div>
// );

export default App;
