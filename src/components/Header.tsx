import React from "react";
import { logo } from "assets/img";
import Navbar from "react-bootstrap/Navbar";

export const Header = ({ username }: { username: string }) => {
  return (
    <Navbar bg="white" expand="lg" className="header shadow-sm">
      <Navbar.Brand href="#home">
        <img src={logo} width="150" height="55" alt="Logo" />
      </Navbar.Brand>
      <Navbar.Collapse className="justify-content-end">
        <Navbar.Text className="text-dark pr-2">Bienvenido (a)</Navbar.Text>
        <Navbar.Text className="text-dark mr-4 font-bold">
          {username}
        </Navbar.Text>
      </Navbar.Collapse>
    </Navbar>
  );
};

export const HeaderQueue = ({ area }: { area: string }) => {
  return (
    <Navbar bg="white" expand="lg" className="header shadow-drl">
      <Navbar.Text className="drl-title text-orange">{area}</Navbar.Text>
      <Navbar.Collapse className="py-3 justify-content-end">
        <Navbar.Brand href="#home">
          <img
            src={logo}
            width="225"
            height="75"
            alt="Logo"
            className="float-right mr-5"
          />
        </Navbar.Brand>
      </Navbar.Collapse>
    </Navbar>
  );
};
