import React from "react";
import {
  Route,
  RouteComponentProps,
  RouteProps,
  Redirect,
} from "react-router-dom";
import { RootState } from "reducers";
import { UserState } from "actions";
import { ReactNode } from "react";
import { connect } from "react-redux";

// Components
import LayoutAdmin from "screens/layout/LayoutAdmin";
import { ROLES, PATH, PATH_QUEUE } from "utils";

interface PrivateRouteProps extends RouteProps {
  component:
    | React.ComponentType<RouteComponentProps<any>>
    | React.ComponentType<any>;
}

const mapState = (state: RootState) => ({
  user: state.user,
});

type RenderComponent = (props: RouteComponentProps<any>) => React.ReactNode;

class PrivateRoute extends Route<
  PrivateRouteProps & {
    user: UserState;
  }
> {
  render(): ReactNode {
    const { component: Component, ...rest }: PrivateRouteProps = this.props;
    const { user, path } = this.props;
    const isAuthenticated = user;

    const hasAccess =
      4 === ROLES.USER && !!PATH.find((name) => name === path);

    const PageComponent = (props: RouteComponentProps) =>
      path === PATH_QUEUE ? (
        <Component {...props} />
      ) : (
        <LayoutAdmin>
          <Component {...props} />
        </LayoutAdmin>
      );

    const renderComponent: RenderComponent = (props) =>
      !!isAuthenticated && !hasAccess ? (
        <PageComponent {...props} />
      ) : (
        <Redirect to="/login" />
      );

    return <Route {...rest} render={renderComponent} />;
  }
}

export default connect(mapState)(PrivateRoute);
